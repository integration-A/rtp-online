package com.flipkrt.orders.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/a")
public class AResource {
    private String health ="service A is running fine......";
    @Path("/health")
    @GET
    @Produces("text/html")
    public String healthCheck(){
        return health;

    }


}
