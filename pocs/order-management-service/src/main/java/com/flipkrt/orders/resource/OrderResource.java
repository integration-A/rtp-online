package com.flipkrt.orders.resource;

import com.flipkrt.orders.beans.Order;

import javax.annotation.PostConstruct;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/orders")
public class OrderResource {
    private String health = "order-management-service is running fine......";

    @Path("/health")
    @GET
    @Produces("text/html")
    public String healthCheck() {
        return health;

    }


    @Path("/create")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    public Response createOrder(Order order,@HeaderParam("clientId") String client_id){

        System.out.println("Header received "+ client_id);
        System.out.println("order details received :"+order);
        return  Response.status(201).entity("order created successfully").build();
    }
    /*@Path("/getOrder")*/
    @Path("/{id}")
    @GET
    @Produces({"application/json","application/xml"})
    public Response getOrder(@PathParam("id") Integer  orderId) {

        Order order = new Order();
        //it will connect database from here
        if (orderId.equals("10")) {


            order.setId(orderId);
            order.setOrderName("keyboards");
            order.setDeliveryAddress("Hyderabad");

        }


        return  Response.status(201).entity(order).build();
    }




    public static void main(String[] args) {
        OrderResource orderResource = new OrderResource();
        System.out.println(orderResource.healthCheck());
        //shortcut for formatting code  : ctr+alt+l
       /* Order order = orderResource.getOrder("flpkart10");
        System.out.println("order details :" + order.getOrderName());
        System.out.println(order.getDeliveryAddress());*/

    }

}
